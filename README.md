# orgmembers

Given a Zendesk ticket number, show name & email of its members\
The ticket creator must be associated with an organization

Required ENV variables: ZD_URL, ZD_USERNAME, and ZD_TOKEN

Usage: `./orgmembers.rb <ticket#> [csv]`

Examples:\
Output to screen:   `./orgmembers.rb 123456`\
Output to csv file: `./orgmembers.rb 123456 csv`

CSV file name is `<ticket#>.csv`

Zendesk API load: 3 calls

`GET /organizations/{org_id}/organization_memberships` returns max 100 records\
`GET /users/show_many` can handle max 100 ids\
We currently support max 30 support contacts, so script will work fine without pagination