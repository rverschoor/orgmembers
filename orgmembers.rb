#!/usr/bin/env ruby
# frozen_string_literal: true

# Given a Zendesk ticket number, show name & email of its members
# The ticket creator must be associated with an organization
#
# Required ENV variables: ZD_URL, ZD_USERNAME, and ZD_TOKEN
#
# Zendesk API load: 3 calls
#
# GET /organizations/{org_id}/organization_memberships returns max 100 records
# GET /users/show_many can handle max 100 ids
# We currently support max 30 support contacts, so script will work fine without pagination

VERSION = 1.0

require 'bundler/inline'
gemfile do
  source 'https://rubygems.org'
  gem 'faraday'
  gem 'faraday-retry'
end

require 'csv'
require 'faraday'

# Zendesk API service
class Api
  def initialize
    @conn = Faraday.new(ENV.fetch('ZD_URL')) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch('ZD_USERNAME')}/token", ENV.fetch('ZD_TOKEN')
      # config.response :logger, nil, { headers: false, bodies: false }  # Log request
    end
  end

  def get(endpoint, params = nil)
    resp = @conn.get endpoint, params
    handle_status resp
    handle_body resp
  end

  private

  def retry_options
    { retry_statuses: [429] } # Retry when hitting rate limit
  end

  def handle_body(resp)
    resp.body
  end

  def handle_status(resp)
    return unless resp.status != 200
    puts "FAIL: Status #{resp.status}"
    exit 2
  end
end

# Zendesk API endpoints
class Zendesk
  def initialize
    @api = Api.new
  end

  def ticket_info(id)
    @api.get "tickets/#{id}"
  end

  def org_memberships(org_id)
    @api.get "organizations/#{org_id}/organization_memberships"
  end

  def user_info(ids)
    @api.get "users/show_many?ids=#{ids.join(',')}"
  end
end

# Application main class
class App
  def initialize
    @zd = Zendesk.new
    @csv_output = false
    usage unless parse_commandline
  end

  def go
    ticket_info = @zd.ticket_info @ticket_id
    org_id = extract_organization_id ticket_info
    memberships = @zd.org_memberships org_id
    user_ids = extract_user_ids memberships
    user_info = @zd.user_info user_ids
    show user_info
    csv user_info
  end

  private

  def parse_commandline
    args_ok = false
    len = ARGV.length
    if [1, 2].include?(len)
      @ticket_id = ARGV[0]
      args_ok = parse_csv_output
    end
    args_ok
  end

  def parse_csv_output
    arg2_ok = true
    if ARGV.length == 2
      if ARGV[1].downcase == 'csv'
        @csv_output = true
      else
        arg2_ok = false
      end
    end
    arg2_ok
  end

  def usage
    puts "=== orgmembers v#{VERSION} ==="
    puts 'Show members of a Zendesk organization'
    puts 'Required ENV variables: ZD_URL, ZD_USERNAME, and ZD_TOKEN'
    puts 'Usage: ./app.rb <ticket#> [csv]'
    puts 'Examples:'
    puts '  Output to screen:   ./app.rb 123456'
    puts '  Output to csv file: ./app.rb 123456 csv'
    puts 'CSV file name is <ticket#>.csv'
    exit 1
  end

  def extract_organization_id(ticket)
    ticket['ticket']['organization_id']
  end

  def extract_user_ids(memberships)
    memberships['organization_memberships'].map do |membership|
      membership['user_id']
    end
  end

  def show(info)
    puts "#{info['users'].length} users"
    puts 'Name - email'
    info['users'].each do |user|
      puts "#{user['name']} - #{user['email']}"
    end
  end

  def csv(info)
    return unless @csv_output
    fname = "#{@ticket_id}.csv"
    header = %w[name email]
    CSV.open(fname, 'w') do |csv|
      csv << header
      info['users'].each do |user|
        csv << [user['name'], user['email']]
      end
    end
    puts "CSV saved in #{fname}"
  end
end

app = App.new
app.go
